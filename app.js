const Rx = rxjs;
!function () {
  function polyfillOperator(name) {
    Rx.Observable.prototype[name] = function () {
      return Rx.operators[name].call(this, arguments)(this);
    };
  }

  function polyfillObservable(name) {
    Rx.Observable[name] = Rx[name];
  }

  polyfillObservable('create');
  polyfillObservable('fromEvent');
  polyfillObservable('merge');
  polyfillOperator('debounceTime');
}();

!function() {
  const SET_HOST = Symbol('SET_HOST');
  const SET_X_AXIS = Symbol('SET_X_AXIS');
  const SET_Y_AXIS = Symbol('SET_Y_AXIS');

  function reducers(state, action) {
    switch (action.type) {
      case SET_HOST:
        return Object.assign({}, state, { hostname: action.value });
      case SET_X_AXIS:
        return Object.assign({}, state, { x: action.value });
      case SET_Y_AXIS:
        return Object.assign({}, state, { y: action.value });
      default:
        return state;
    }
  }

  const store = Redux.createStore(reducers, { hostname: 'localhost:53317', x: 0, y: 0 });

  store.actions = {
    setHost(hostname) {
      return {
        value: hostname,
        type: SET_HOST,
      }
    },
    setX(x) {
      return {
        value: x,
        type: SET_X_AXIS,
      };
    },
    setY(y) {
      return {
        value: y,
        type: SET_Y_AXIS,
      };
    },
  };

  window.store = store;
}();

!function(){
  Rx.Observable.fromEvent(document.getElementById('hostname'), 'input')
    .debounceTime(500)
    .subscribe((event) => {
      store.dispatch(store.actions.setHost(event.target.value));
    });

  Rx.Observable.fromEvent(document.getElementById('go-live'), 'click')
    .debounceTime(500)
    .subscribe(() => {
      const { hostname } = store.getState();
      axios.get(`http://${hostname}/connectStream`);
    });

  Rx.Observable.fromEvent(document.getElementById('stop-live'), 'click')
    .debounceTime(500)
    .subscribe(() => {
      const { hostname } = store.getState();
      axios.get(`http://${hostname}/disconnectStream`);
    });

  {
    const range = document.querySelector('[data-direction=horizontal] input[type=range]');
    const number = document.querySelector('[data-direction=horizontal] input[type=number]');
    Rx.Observable.merge(
      Rx.Observable.fromEvent(range, 'input'),
      Rx.Observable.fromEvent(number, 'input'),
    )
    .subscribe(({ target: { value } }) => {
      range.value = number.value = value;
      store.dispatch(store.actions.setX(value))
    });
  }

  {
    const range = document.querySelector('[data-direction=vertical] input[type=range]');
    const number = document.querySelector('[data-direction=vertical] input[type=number]');
    Rx.Observable.merge(
      Rx.Observable.fromEvent(range, 'input'),
      Rx.Observable.fromEvent(number, 'input'),
    )
    .subscribe(({ target: { value } }) => {
      range.value = number.value = value;
      store.dispatch(store.actions.setY(value))
    });
  }

    Rx.Observable.merge(
      new Rx.Observable((observer) => {
        return store.subscribe(function () {
          observer.next(store.getState());
        });
      }),
      Rx.Observable.fromEvent(document.getElementById('send'), 'click'),
    )
    .debounceTime(2000)
    .subscribe(() => {
      const { hostname, x, y } = store.getState();
      const param = new URLSearchParams();
      param.set('yaw', x);
      param.set('pitch', y);
      param.set('time', '2');
      param.set('isRelative', '0');
      const url = `http://${hostname}/move?${param.toString()}`;
      axios.get(url);
    });
}();
